Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsSesionCajasExtraordinario

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

    Public Function fListarArea(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    Public Function fListarNumeroCaja(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGenerarNumeroSesion(ByVal vEmprCodigo As String, ByVal vIdCaja As String, ByVal vAreaCodigo As String, ByVal vPeriodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, vPeriodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
            'sCadena = IIf(IsDBNull(cCapaDatos.fCmdExecuteScalar()), 0, cCapaDatos.fCmdExecuteScalar())
            If dtTable.Rows.Count = 0 Then
                sCadena = 1
            Else
                sCadena = Val(dtTable.Rows(0).Item("NumSesion"))
                sCadena += 1
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fNroCajaExtraordinariaxArea(ByVal vEmprCodigo As String, ByVal vAreaCodigo As String, ByVal vPeriodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, vPeriodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
            'sCadena = IIf(IsDBNull(cCapaDatos.fCmdExecuteScalar()), 0, cCapaDatos.fCmdExecuteScalar())
            If dtTable.Rows.Count = 0 Then
                sCadena = 1
            Else
                sCadena = Val(dtTable.Rows(0).Item("NroCaja"))
                'sCadena += 1
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerResponsable(ByVal vCodigoCaja As String, ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vCodigoCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fActivaryDesActivarSesion(ByVal iOpcion As Integer, ByVal IdSesion As String, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function




    Public Function fTraerCajasCerradas(ByVal CodEmpresa As String, ByVal periodo As String, ByVal area As String, ByVal IdCaja As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, area)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCabeceraDePrimerRegistro(ByVal vNumeroSesion As Integer, ByVal IdCaja As String, ByVal vAreaCodigo As String, ByVal vEmprCodigo As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, vNumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@IdEmpleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            'cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCab(ByVal vNumeroSesion As Integer, ByVal vAreaCodigo As String, ByVal IdCaja As String, ByVal vEmprCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, vNumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@IdEmpleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            'cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    'Public Function fExisteAporteInicial(ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vEstado As Integer) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, 0)
    '        cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
    '        'detalle
    '        cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
    '        cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
    '        'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
    '        'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
    '        'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
    '        'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
    '        cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
    '        'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function


    Public Function fExisteAporteInicial(ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vEstado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDet(ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vEstado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 20)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fBuscarComprob(ByVal iOpcion As Integer, ByVal vIdEmpresa As String, ByVal Texto As String, ByVal FIni As DateTime, ByVal FFin As DateTime, ByVal Contable As Integer, ByVal Prestamo As Integer, ByVal Periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaCajas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 20, Texto)
            cCapaDatos.sParametroSQL("@F1", DbType.DateTime, 10, Format(FIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@F2", DbType.DateTime, 10, Format(FFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Contable", DbType.Int32, 20, Contable)
            cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 20, Prestamo)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarGastosCaja(ByVal vIdSesion As String, ByVal vIdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspImprimirCajaChicaExtraordinaria10"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fBuscarReembolso(ByVal vIdSesion As String, ByVal vIdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBuscarReembolsoSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDetCerrado(ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vEstado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDetallesDeHoy(ByVal vIdSesion As String, ByVal vFecha As DateTime, ByVal vArea As String, ByVal vCaja As String, ByVal vIdEmpresa As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vArea)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistrosDetHoy = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function ExisteSaldoAntHoy(ByVal vIdSesion As String, ByVal vFecha As DateTime, ByVal vArea As String, ByVal vCaja As String, ByVal vIdEmpresa As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 33)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vArea)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerUltimoSesionDeCaja(ByVal vIdCaja As String, ByVal vIdArea As String, ByVal vCodEmpresa As String, ByVal Periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vIdArea)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodEmpresa)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fTraerDatosUltimaSesion(ByVal vAreaCodigo As String, ByVal IdCaja As String, ByVal vEmprCodigo As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 24)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fTraerDatosUltimaSesion2(ByVal vAreaCodigo As String, ByVal IdCaja As String, ByVal vEmprCodigo As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerCantidadSesionesDeCaja(ByVal IdCaja As String, ByVal vAreaCodigo As String, ByVal vEmprCodigo As String, ByVal Periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo(ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@IdEmpleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    '*********************************************************************
    '*********************************************************************

    Public Function fCargarTipoGastoss(ByVal iOpcion As Integer, ByVal sCodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, sCodEmpr)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function fCargarTipoGastos() As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    'Public Function fCargarTipoGastos2() As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    'Public Function fCargarTipoGastos3() As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    'Public Function fCargarTipoGastos4() As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    Public Function fListarEstadoArqueo() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarCCosto(ByVal vCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)


        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarCCostoForms(ByVal iOpcion As Integer, ByVal vCodEmpresa As String, ByVal Criterio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, Criterio)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarTipoMovimiento() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarTipoDocumento() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarEmpresas(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdSesion As String, ByVal vNumeroSesion As Integer, ByVal vAreaCodigo As String, ByVal vPadCodigo As String, ByVal vIdCaja As String,
                            ByVal vFechaInicio As DateTime, ByVal vFechaFin As DateTime, ByVal vMontoInicioSoles As Decimal, ByVal vMontoTotalGas As Decimal, ByVal vMontoFinSoles As Decimal,
                            ByVal vCerraCaja As Integer, ByVal vEmprCodigo As String, ByVal vOpcion As Integer, ByVal DTBL As DataTable, ByVal Reembolso As Decimal, ByVal Periodo As String,
                            ByVal TPlaCodigo As String, ByVal EmprResposable As String, ByVal vMontoInicioSolesD As Decimal, ByVal vMontoTotalGasD As Decimal, ByVal vMontoFinSolesD As Decimal,
                            ByVal sUsuario As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, vNumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, vPadCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(vFechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(vFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, Format(vMontoInicioSoles, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, Format(vMontoTotalGas, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, Format(vMontoFinSoles, "##,##0.00"))
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(Reembolso, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, vCerraCaja)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, TPlaCodigo)
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, sUsuario)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, EmprResposable)
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(vMontoInicioSolesD, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(vMontoTotalGasD, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(vMontoFinSolesD, "##,##0.00"))
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Try
            For x As Integer = 0 To DTBL.Rows.Count - 1
                fCodigoIdDetalleMovimiento(vIdSesion, vEmprCodigo) 'Genera IdDetalleMovimiento Nuevo vIdSesion vEmprCodigo
                'Fecha
                Dim vFecha As DateTime
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(0)) = True Then
                    vFecha = Today()
                Else
                    vFecha = DTBL.Rows(x).Item(0)
                End If
                'IdDocumento
                Dim vDocumento As String
                vDocumento = DTBL.Rows(x).Item(1)
                'NroDoc
                Dim vDocumentoReferencia As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(2)) = True Then
                    vDocumentoReferencia = ""
                Else
                    vDocumentoReferencia = DTBL.Rows(x).Item(2)
                End If
                ' sepuede resumir  asi
                ' vDocumentoReferencia = IIf(IsDBNull(DTBL.Rows(x).Item(2)), "", DTBL.Rows(x).Item(2))
                'IdTipoGasto
                Dim vGasto As String
                vGasto = DTBL.Rows(x).Item(3)
                'Glosa
                Dim vDescripcion As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(4)) = True Then
                    vDescripcion = ""
                Else
                    vDescripcion = DTBL.Rows(x).Item(4)
                End If
                'IdCentroCosto
                Dim vCentroCosto As String
                vCentroCosto = DTBL.Rows(x).Item(5)
                ''''''''''
                If vNumeroSesion > 1 And vGasto = "1" Then
                    Dim rActuCab As Int16 = 0
                    rActuCab = fActualizarCabecera2(6, Trim(vIdSesion), vEmprCodigo, vNumeroSesion, Trim(vAreaCodigo), Trim(vIdCaja), vFechaInicio, vFechaFin, vMontoInicioSoles, vMontoTotalGas, vMontoFinSoles, vCerraCaja, sUsuario)
                End If
                ''''''''''
                'ImporteDoc
                Dim vTotal As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(6)) = True Then
                    vTotal = "0.00"
                Else
                    vTotal = DTBL.Rows(x).Item(6)
                End If
                'Eliminado SI/NO
                Dim vAnulado As Integer
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(9)) = True Then
                    vAnulado = ""
                Else
                    vAnulado = DTBL.Rows(x).Item(9)
                End If
                'NroVale
                Dim vVale As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(10)) = True Then
                    vVale = ""
                Else
                    vVale = DTBL.Rows(x).Item(10)
                End If
                'CodEmpresa
                Dim vEmpresaPrestada As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(11)) = True Then
                    vEmpresaPrestada = ""
                Else
                    vEmpresaPrestada = DTBL.Rows(x).Item(11)
                End If
                'EstadoPrestamo
                Dim vEstPrestamo As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(12)) = True Then
                    vEstPrestamo = ""
                Else
                    vEstPrestamo = DTBL.Rows(x).Item(12)
                End If
                'Ruc
                Dim vRuc As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(13)) = True Then
                    vRuc = ""
                Else
                    vRuc = DTBL.Rows(x).Item(13)
                End If
                'RazonSocial
                Dim vRazonSocial As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(14)) = True Then
                    vRazonSocial = ""
                Else
                    vRazonSocial = DTBL.Rows(x).Item(14)
                End If
                'OtrosDatos
                Dim vOtrosDatos As String
                If Microsoft.VisualBasic.IsDBNull(DTBL.Rows(x).Item(15)) = True Then
                    vOtrosDatos = ""
                Else
                    vOtrosDatos = DTBL.Rows(x).Item(15)
                End If


                cCapaDatos.sConectarSQL()
                sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
                cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
                cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
                cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
                cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
                cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
                cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
                cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, sUsuario)
                cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
                cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
                cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(Reembolso, "##,##0.00"))
                cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
                cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
                'detalle
                cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, sCodIdDetalleMovimientoFuturo)
                cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, vDocumento)
                cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, vIdSesion)
                cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format("yyyy-MM-dd", vFecha))
                cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
                cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCentroCosto)
                cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
                cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, vGasto)
                cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, vDocumentoReferencia)
                cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
                cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
                cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, vVale)
                cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, vEmpresaPrestada)
                cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, vEstPrestamo)
                cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")

                cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, vRuc)
                'cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
                cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, vRazonSocial)
                cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, vOtrosDatos)
                cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                iResultadoDetalle = cCapaDatos.fCmdExecuteNonQuery()
                cCapaDatos.fLimpiarParametros()
            Next
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoDetalle
    End Function

    Public Function VerificarDatos(ByVal vOpcion As Integer, ByVal vFecha As DateTime, ByVal vTipoDoc As String, ByVal vNumeroDoc As String, ByVal vTipoGasto As String, ByVal vDescripcion As String, ByVal vCentroCosto As String, ByVal vTotal As Decimal, ByVal vIdDetalleMovimiento As String, ByVal vAnulado As Integer, ByVal vVale As String, ByVal pIdSesion As String, ByVal xgEmpresa As String, ByVal vEmprPrestada As String, ByVal vEstadoPrestamo As Integer, ByVal Ruc As String, ByVal RazonSocial As String, ByVal OtrosDatos As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(vFecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, vTipoDoc)
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, vNumeroDoc)
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, vTipoGasto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, vCentroCosto)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, vIdDetalleMovimiento)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, pIdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, xgEmpresa)

            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 0)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, vVale)
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, vEmprPrestada)
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, vEstadoPrestamo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, RazonSocial)
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, OtrosDatos)
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ActualizarDatos(ByVal vOpcion As Integer, ByVal vFecha As DateTime, ByVal vTipoDoc As String, ByVal vNumeroDoc As String, ByVal vTipoGasto As String, ByVal vDescripcion As String, ByVal vCentroCosto As String, ByVal vTotal As Decimal, ByVal vIdDetalleMovimiento As String, ByVal vAnulado As Integer, ByVal vVale As String, ByVal pIdSesion As String, ByVal xgEmpresa As String, ByVal vEmprPrestada As String, ByVal vEstadoPrestamo As Integer, ByVal Ruc As String, ByVal RazonSocial As String, ByVal OtrosDatos As String, ByVal sUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(vFecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, vTipoDoc)
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, vNumeroDoc)
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, vTipoGasto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, vCentroCosto)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, vIdDetalleMovimiento)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, pIdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, xgEmpresa)

            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, Trim(sUsuario))
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 0)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, vVale)
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, vEmprPrestada)
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, vEstadoPrestamo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, RazonSocial)
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, OtrosDatos)
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)

            cCapaDatos.sAdaptadorSQL("R")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabarItemNuevo(ByVal vOpcion As Integer, ByVal vFecha As DateTime, ByVal vTipoDoc As String, ByVal vNumeroDoc As String, ByVal vTipoGasto As String, ByVal vDescripcion As String, ByVal vCentroCosto As String, ByVal vTotal As Decimal, ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vAnulado As Integer, ByVal vVale As String, ByVal vEmprPrestada As String, ByVal vEstadoPrestamo As Integer, ByVal Ruc As String, ByVal RazonSocial As String, ByVal OtrosDatos As String, ByVal sUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            fCodigoIdDetalleMovimiento(vIdSesion, vIdEmpresa) 'Genera IdDetalleMovimiento Nuevo
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(vFecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, vTipoDoc)
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, vNumeroDoc)
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, vTipoGasto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, vCentroCosto)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, sCodIdDetalleMovimientoFuturo)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, vIdEmpresa)

            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, Trim(sUsuario))
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 0)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, vVale)
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, vEmprPrestada)
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, vEstadoPrestamo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, RazonSocial)
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, OtrosDatos)
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActualizarCabecera(ByVal vOpcion As Integer, ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vNumeroSesion As Integer, ByVal vAreaCodigo As String, ByVal vIdCaja As String, ByVal vFechaInicio As DateTime, ByVal vFechaFin As DateTime, ByVal vMontoInicioSoles As Decimal, ByVal vMontoTotalGas As Decimal, ByVal vMontoFinSoles As Decimal, ByVal vCerrarCaja As Integer, ByVal vMontoInicioSolesD As Decimal, ByVal vMontoTotalGasD As Decimal, ByVal vMontoFinSolesD As Decimal, ByVal sUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            'fCodigoIdDetalleMovimiento() 'Genera IdDetalleMovimiento Nuevo
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)

            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, sUsuario)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, vIdEmpresa)

            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, vCerrarCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(vFechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(vFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, vMontoInicioSoles)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, vMontoTotalGas)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, vMontoFinSoles)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, vNumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 0)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, vMontoInicioSolesD)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, vMontoTotalGasD)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, vMontoFinSolesD)

            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActualizarCabecera2(ByVal vOpcion As Integer, ByVal vIdSesion As String, ByVal vIdEmpresa As String, ByVal vNumeroSesion As Integer, ByVal vAreaCodigo As String, ByVal vIdCaja As String, ByVal vFechaInicio As DateTime, ByVal vFechaFin As DateTime, ByVal vMontoInicioSoles As Decimal, ByVal vMontoTotalGas As Decimal, ByVal vMontoFinSoles As Decimal, ByVal vCerrarCaja As Integer, ByVal sUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, sUsuario)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, vCerrarCaja)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(vFechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(vFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, vMontoInicioSoles)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, vNumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, vCerrarCaja)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function VerificarDatosLetra(ByVal vOpcion As Integer, ByVal IdLetra As String, ByVal IdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdLetra)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 0)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fActualizarLetras(ByVal vOpcion As Integer, ByVal Estado As Integer, ByVal IdLetra As String, ByVal IdEmpresa As String, ByVal SaldoLetra As Double, ByVal IdUsuario As String, ByVal FechaGiro As DateTime, ByVal FechaVen As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            'fCodigoIdDetalleMovimiento()'Genera IdDetalleMovimiento Nuevo
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, IdUsuario)
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdLetra)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaGiro, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(FechaVen, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, SaldoLetra)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, Estado)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    'Public Function ActualizarDatos(ByVal vOpcion As Integer, ByVal vFecha As DateTime, ByVal vTipoDoc As String, ByVal vNumeroDoc As String, ByVal vTipoGasto As String, ByVal vDescripcion As String, ByVal vCentroCosto As String, ByVal vTotal As Decimal, ByVal vIdDetalleMovimiento As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
    '        cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(vFecha, "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, vTipoDoc)
    '        cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, vNumeroDoc)
    '        cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, vTipoGasto)
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
    '        cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 2, vCentroCosto)
    '        cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
    '        cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, vIdDetalleMovimiento)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function


    'Public Function fGrabarItemNuevo(ByVal vOpcion As Integer, ByVal vFecha As DateTime, ByVal vTipoDoc As String, ByVal vNumeroDoc As String, ByVal vTipoGasto As String, ByVal vDescripcion As String, ByVal vCentroCosto As String, ByVal vTotal As Decimal, ByVal vIdSesion As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        fCodigoIdDetalleMovimiento() 'Genera IdDetalleMovimiento Nuevo
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
    '        cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(vFecha, "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, vTipoDoc)
    '        cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, vNumeroDoc)
    '        cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, vTipoGasto)
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, vDescripcion)
    '        cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 2, vCentroCosto)
    '        cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, vTotal)
    '        cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, sCodIdDetalleMovimientoFuturo)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    Public Function fCodigoIdDetalleMovimiento(ByVal vIdSesion As String, ByVal vEmprCodigo As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 24)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodIdDetalleMovimientoFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoVale(ByVal iOpcion As Integer, ByVal IdSesion As String, ByVal IdCaja As String, ByVal AreaCodigo As String, ByVal IdEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodVale = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabarVale(ByVal ValeID As String, ByVal iOpcion As Integer, ByVal IdSesion As String, ByVal IdCaja As String, ByVal AreaCodigo As String, ByVal IdEmpresa As String) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function fActVale(ByVal ValeID As String, ByVal IdSesion As String, ByVal IdEmpresa As String, ByVal ANombre As String, ByVal Total As Double) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, ANombre)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, Total)
            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function fActVale2(ByVal ValeID As String, ByVal IdSesion As String, ByVal IdEmpresa As String, ByVal ADescrip As String, ByVal Estado As Integer) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, ADescrip)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)

            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function fActVale3(ByVal ValeID As String, ByVal IdSesion As String, ByVal IdEmpresa As String, ByVal ANombre As String, ByVal Total As Double) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, ANombre)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, Total)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function fActVale4(ByVal ValeID As String, ByVal IdSesion As String, ByVal IdEmpresa As String, ByVal ANombre As String, ByVal Total As Double) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, ANombre)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, Total)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function fActVale5(ByVal ValeID As String, ByVal IdSesion As String, ByVal IdEmpresa As String, ByVal ANombre As String, ByVal Total As Double) As Integer
        Dim iResultadoVale As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, ValeID)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, ANombre)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, Total)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            iResultadoVale = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoVale
    End Function

    Public Function BuscarDatosdeVale(ByVal IdVale As String, ByVal IdSesion As String, ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, IdVale)
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function BuscarNombres(ByVal IdCaja As String, ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function BuscarVales(ByVal IdSesion As String, ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspVales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@ValeID", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ANombre", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@ADescrip", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function fCodigoNuevoVale() As String
    '    Dim iCodigo As String = ""
    '    Dim sSufijo As String = "00000000000000000000"
    '    cCapaDatos = New clsCapaDatos
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 50)
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
    '        If iCodigo = "" Then
    '            iCodigo = 1
    '        Else
    '            iCodigo += 1
    '        End If
    '        sCodIdDetalleMovimientoFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return iCodigo
    'End Function

    Public Function fGasto(ByVal cadena As String) As String
        Dim IdGasto As String
        If cadena = "" Then
            IdGasto = ""
        Else
            IdGasto = cadena.Substring(0, cadena.IndexOf(" ")).Trim
        End If
        Return IdGasto
    End Function

    Public Function fDocumento(ByVal cadena As String) As String
        Dim IdDocumento As String
        If cadena = "" Then
            IdDocumento = ""
        Else
            IdDocumento = cadena.Substring(0, cadena.IndexOf(" ")).Trim
        End If
        Return IdDocumento
    End Function

    Public Function fTipoMovimiento(ByVal cadena As String) As String
        Dim IdTipoMovCorto As String
        If cadena = "" Then
            IdTipoMovCorto = ""
        Else
            IdTipoMovCorto = cadena.Substring(0, cadena.IndexOf("-")).Trim
        End If
        Return IdTipoMovCorto
    End Function

    Public Function fCentroCosto(ByVal cadena As String) As String
        Dim IdCentroCosto As String
        If cadena = "" Then
            IdCentroCosto = ""
        Else
            IdCentroCosto = cadena.Substring(0, cadena.IndexOf(" ")).Trim
        End If
        Return IdCentroCosto
    End Function

    Public Function fEmpresaPrestada(ByVal cadena As String) As String
        Dim IdEmpresa As String
        If cadena = "" Then
            IdEmpresa = ""
        Else
            IdEmpresa = cadena.Substring(0, cadena.IndexOf(" ")).Trim
        End If
        Return IdEmpresa
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal vEstado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, sCodIdDetalleMovimientoFuturo)
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function



    Public Function fListarCaja() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, sCodIdDetalleMovimientoFuturo)
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    'Public Function fSeleccionarTipoCambio(ByVal VFechaTipoCambio As DateTime) As String
    '    cCapaDatos = New clsCapaDatos
    '    Dim sResultado As String = ""

    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 10)
    '        cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, 0)
    '        cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@TcaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdEstadoSesion", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(VFechaTipoCambio, "yyyy-MM-dd"))
    '        sResultado = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))

    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return sResultado
    'End Function

    'Public Function fSeleccionarTcaVenta() As String
    '    cCapaDatos = New clsCapaDatos
    '    Dim sResultado As String = ""

    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 11)
    '        cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, 0)
    '        cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@TcaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdEstadoSesion", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

    '        sResultado = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))

    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return sResultado
    'End Function

    Public Function fListarEmpresa() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarArea() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarEmpleado() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fCargarProveedor() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

            ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fValidarIngreso(ByVal vNumeroSesion As Integer) As Boolean
        cCapaDatos = New clsCapaDatos
        Dim dtlistar As New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, vNumeroSesion)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            ' ''detalle
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtlistar = cCapaDatos.fListarTabla()

            If dtlistar.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

    End Function

    Public Function fActualizar(ByVal VIdSesion As String, ByVal VNumeroSesion As Integer, ByVal Vareacodigo As String, ByVal VIdCaja As String, ByVal VFechaInicio As DateTime, ByVal VFechaFin As DateTime, ByVal VMontoInicioSoles As Decimal, ByVal VMontoTotalGas As Decimal, ByVal VMontoFinSoles As Decimal, ByVal VCerrarCaja As String, ByVal VEmprCodigo As String, ByVal DTBL2 As DataTable) As Integer


        'If DGV.Rows.Count > 0 Then

        'End If

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            'cabecera�()
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, VIdSesion)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, VNumeroSesion)
            cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, Vareacodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, VIdCaja)
            ' cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(VFechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(VFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, VMontoInicioSoles)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, VMontoTotalGas)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, VMontoFinSoles)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, VCerrarCaja)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, VEmprCodigo)

            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.fCmdExecuteNonQuery()
            ''2DO
            ' ''cabecera�
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            For x As Integer = 0 To DTBL2.Rows.Count - 1
                cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 24)
                cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
                cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
                cCapaDatos.sParametroSQL("@areacodigo", DbType.String, 2, "")
                cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
                'cCapaDatos.sParametroSQL("@idempleado", DbType.String, 50, "")
                cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
                'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
                cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
                cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
                cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
                cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
                cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
                cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
                cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
                cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
                cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
                cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
                cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
                cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
                cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
                cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
                '' ''detalle

                'extraer valores
                Dim VDocumento As String
                VDocumento = fDocumento(DTBL2.Rows(x).Item(2))

                Dim VGasto As String
                VGasto = fGasto(DTBL2.Rows(x).Item(4))

                Dim VTipoMovimiento As String
                VTipoMovimiento = fTipoMovimiento(DTBL2.Rows(x).Item(6))

                Dim VCentroCosto As String
                VCentroCosto = fCentroCosto(DTBL2.Rows(x).Item(7))

                cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, DTBL2.Rows(x).Item(0))
                cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, DTBL2.Rows(x).Item(1))
                cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, VDocumento)
                'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, DTBL2.Rows(x).Item(3))
                cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, VGasto)
                'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, DTBL2.Rows(x).Item(5))
                cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, VTipoMovimiento)
                cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, VCentroCosto)
                cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, DTBL2.Rows(x).Item(8))
                'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, IIf(DTBL2.Rows(x).Item(9) = 1, 1, 0))
                cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, DTBL2.Rows(x).Item(10))

                cCapaDatos.fCmdExecuteNonQuery()
                cCapaDatos.fLimpiarParametros()

            Next

            iResultado = 1

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function



    Public Function fEliminarItemDetalle(ByVal Codigo As String, ByVal IdSesion As String, ByVal CodEmpresa As String, ByVal sUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspModificarDetalleMovimientoExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoDoc", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@CentroCosto", DbType.String, 10, sUsuario)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, "00.0")
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdEmpresa", DbType.String, 2, CodEmpresa)

            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesionActual)
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int16, 1, 1)
            ''''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado = 0 Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarCajasPorArea(ByVal vCodigoArea As String, ByVal vCodigoEmpresa As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCargaCombosSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vCodigoArea)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListReembolsosdeCaja(ByVal vAreaCodigo As String, ByVal IdCaja As String, ByVal vEmprCodigo As String, ByVal periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSesionCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 40)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vAreaCodigo)
            cCapaDatos.sParametroSQL("@PadCodigo", DbType.String, 10, "")
            'cCapaDatos.sParametroSQL("@IdEmpleado", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            'cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoInicioSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotalGas", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFinSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Reembolso", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@CerrarCaja", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            'detalle
            'cCapaDatos.sParametroSQL("@Item", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdDetalleMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaGasto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NumDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGasto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@DescGasto", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            'cCapaDatos.sParametroSQL("@Prestamo", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IdSesionDet", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            '''''''''''''''
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVale", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprPrestada", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrestamo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@RazonSocial", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OtrosDatos", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@MontoInicioSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoTotalGasD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sParametroSQL("@MontoFinSolesD", DbType.Decimal, 20, Format(0, "##,##0.00"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class

