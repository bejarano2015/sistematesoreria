Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsArqueo

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fCodigo() As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspArqueo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@IdArqueo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoArqueo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoTotalManual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@AperturaDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@GastosDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Sobrante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Faltante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Encargado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Revisado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 100, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal vIdArqueo As String, ByVal vIdSesion As String, ByVal vIdEstadoArqueo As String, ByVal vIdCaja As String, ByVal vIdArea As String, ByVal vIdEmpresa As String, ByVal vMontoTotalManual As Decimal, ByVal vAperturaDia As Decimal, ByVal vGastosDia As Decimal, ByVal vSaldoDia As Decimal, ByVal vSobrante As Decimal, ByVal vFaltante As Decimal, ByVal vEncargado As String, ByVal vRevisado As String, ByVal vObservacion As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspArqueo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@IdArqueo", DbType.String, 20, vIdArqueo)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@IdEstadoArqueo", DbType.String, 2, vIdEstadoArqueo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vIdArea)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoTotalManual", DbType.Decimal, 20, vMontoTotalManual)
            cCapaDatos.sParametroSQL("@AperturaDia", DbType.Decimal, 20, vAperturaDia)
            cCapaDatos.sParametroSQL("@GastosDia", DbType.Decimal, 20, vGastosDia)
            cCapaDatos.sParametroSQL("@SaldoDia", DbType.Decimal, 20, vSaldoDia)
            cCapaDatos.sParametroSQL("@Sobrante", DbType.Decimal, 20, vSobrante)
            cCapaDatos.sParametroSQL("@Faltante", DbType.Decimal, 20, vFaltante)
            cCapaDatos.sParametroSQL("@Encargado", DbType.String, 100, vEncargado)
            cCapaDatos.sParametroSQL("@Revisado", DbType.String, 100, vRevisado)
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 100, vObservacion)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function ActualizarArqueoHoy(ByVal vIdArqueo As String, ByVal vIdEstadoArqueo As String, ByVal vMontoTotalManual As Decimal, ByVal vAperturaDia As Decimal, ByVal vGastosDia As Decimal, ByVal vSaldoDia As Decimal, ByVal vSobrante As Decimal, ByVal vFaltante As Decimal, ByVal vEncargado As String, ByVal vRevisado As String, ByVal vObservacion As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspArqueo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@IdArqueo", DbType.String, 20, vIdArqueo)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoArqueo", DbType.String, 2, vIdEstadoArqueo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoTotalManual", DbType.Decimal, 20, vMontoTotalManual)
            cCapaDatos.sParametroSQL("@AperturaDia", DbType.Decimal, 20, vAperturaDia)
            cCapaDatos.sParametroSQL("@GastosDia", DbType.Decimal, 20, vGastosDia)
            cCapaDatos.sParametroSQL("@SaldoDia", DbType.Decimal, 20, vSaldoDia)
            cCapaDatos.sParametroSQL("@Sobrante", DbType.Decimal, 20, vSobrante)
            cCapaDatos.sParametroSQL("@Faltante", DbType.Decimal, 20, vFaltante)
            cCapaDatos.sParametroSQL("@Encargado", DbType.String, 100, vEncargado)
            cCapaDatos.sParametroSQL("@Revisado", DbType.String, 100, vRevisado)
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 100, vObservacion)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fTraerSaldoAnteriorDeArqueo(ByVal IdSesionCabecera As String, ByVal gEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspArqueo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@IdArqueo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesionCabecera)
            cCapaDatos.sParametroSQL("@IdEstadoArqueo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, gEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoTotalManual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@AperturaDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@GastosDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Sobrante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Faltante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Encargado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Revisado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 100, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ExisteArqueoHoy(ByVal vIdSesion As String, ByVal vIdCaja As String, ByVal vIdArea As String, ByVal vIdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspArqueo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@IdArqueo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, vIdSesion)
            cCapaDatos.sParametroSQL("@IdEstadoArqueo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, vIdArea)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoTotalManual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@AperturaDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@GastosDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Sobrante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Faltante", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Encargado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Revisado", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 100, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
End Class
